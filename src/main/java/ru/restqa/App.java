package ru.restqa;

import org.glassfish.jersey.server.ResourceConfig;

/**
 * класс приложения для локального запуска веб-сервиса
 */
public class App extends ResourceConfig {
    private static boolean isWorked = false;

    public App() {
        packages("ru.restqa");
        isWorked = true;
    }

    public static boolean isIsWorked() {
        return isWorked;
    }
}
