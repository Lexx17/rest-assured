package ru.restqa;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.restassured.path.json.JsonPath;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * класс веб-сервиса drivers.json
 */
@Path("/drivers.json")
@Produces("application/json; charset=UTF-8")
public class DriversEndpoint {

    private static volatile Map<String, Object> fullData = null;

    private static Map<String, Object> getFullData() {
        Map<String, Object> local = fullData;
        if (local == null) {
            synchronized (DriversEndpoint.class) {
                local = fullData;
                if (local == null) {
                    InputStream inputStream = DriversEndpoint.class.getClassLoader().getResourceAsStream("driversFull.json");
                    JsonPath jsonPath = JsonPath.from(inputStream);
                    fullData = local = jsonPath.getMap("MRData");
                }
            }
        }
        return local;
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    public Response get(@QueryParam("limit") Integer queryLimit, @QueryParam("offset") Integer queryOffset) throws JsonProcessingException {
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> source = getFullData();
        int limit = normInt(queryLimit, 30);
        int offset = normInt(queryOffset, 0);
        for (Map.Entry<String, Object> entry : source.entrySet()) {
            Object value = entry.getValue();
            switch (entry.getKey()) {
                case "limit":
                    value = Integer.toString(limit);
                    break;
                case "offset":
                    value = Integer.toString(offset);
                    break;
                case "DriverTable":
                    value = getDriversTable((Map<String, Object>) entry.getValue(), limit, offset);
                    break;
            }
            data.put(entry.getKey(), value);
        }

        Map<String, Object> result = new HashMap<>();
        result.put("MRData", data);
        return Response.ok(result, MediaType.APPLICATION_JSON).build();
    }

    private Map<String, Object> getDriversTable(Map<String, Object> source, int limit, int offset) {
        if (limit >= 30 && offset == 0) {
            return source;
        }
        List<Map<String, Object>> drivers = new ArrayList<>();
        List<Map<String, Object>> driversSource = (List<Map<String, Object>>) source.get("Drivers");
        int maxInd = Math.min(limit + offset, driversSource.size());
        for (int ind = offset; ind < maxInd; ind++) {
            drivers.add(driversSource.get(ind));
        }

        Map<String, Object> result = new HashMap<>();
        result.put("Drivers", drivers);
        return result;
    }

    private int normInt(Integer value, int defaultValue) {
        if (value == null) {
            return defaultValue;
        }
        return value;
    }
}
