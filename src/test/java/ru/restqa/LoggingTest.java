package ru.restqa;

import io.restassured.RestAssured;
import io.restassured.config.LogConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.filter.log.ErrorLoggingFilter;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import org.apache.commons.io.output.WriterOutputStream;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.io.PrintStream;
import java.io.StringWriter;

import static ru.restqa.Api.apiUrlFull;

/**
 * примеры логирования и использования фильтров
 */
public class LoggingTest {
    @Test
    public void simpleConsoleLog() {
        RestAssured.given()
                .log().all()
                .then().log().all()
                .when()
                .get(apiUrlFull())
                .then()
                .statusCode(200);
    }

    @Test
    public void requestFilter() {
        RestAssured.given()
                .filter(new RequestLoggingFilter())
                .get(apiUrlFull())
                .then()
                .statusCode(200);
    }

    @Test
    public void responseFilter() {
        RestAssured.given()
                .filter(new ResponseLoggingFilter())
                .get(apiUrlFull())
                .then()
                .statusCode(200);
    }

    @Test
    public void multiFilter() {
        RestAssured.given()
                .filter(new RequestLoggingFilter())
                .filter(new ResponseLoggingFilter())
                .get(apiUrlFull())
                .then()
                .statusCode(200);
    }

    @Test
    public void errorFilter() {
        RestAssured.given()
                .filter(new ErrorLoggingFilter())
                .get(apiUrlFull() + "/unknown.json")
                .then()
                .statusCode(Matchers.greaterThanOrEqualTo(400));
    }

    @Test
    public void partialLog() {
        RestAssured.given()
                .param("limit", 5)
                .log().params()
                .response().log().headers()
                .log().body()
                .log().ifStatusCodeMatches(Matchers.greaterThanOrEqualTo(400))
                .when()
                .get(apiUrlFull() + "/unknown.json");
    }

    @Test
    public void logIfFail() {
        RestAssured.given()
                .param("limit", 5)
                .log().ifValidationFails()
                .response().log().ifValidationFails()
                .when()
                .get(apiUrlFull() + "/unknown.json")
                .then()
                .statusCode(Matchers.greaterThanOrEqualTo(400));
    }

    @Test
    public void logToStream() {
        StringWriter stringWriter = new StringWriter();
        PrintStream printStream = new PrintStream(new WriterOutputStream(stringWriter));
        RestAssuredConfig config = RestAssured.config().logConfig(
                new LogConfig().defaultStream(printStream)
        );

        RestAssured.given()
                .config(config)
                .get(apiUrlFull())
                .then()
                .statusCode(200);

        System.out.print("From log stream:\n" + stringWriter.toString());
    }
}
