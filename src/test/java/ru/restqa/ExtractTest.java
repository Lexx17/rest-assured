package ru.restqa;

import io.restassured.RestAssured;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;

import static ru.restqa.Api.apiUrlFull;

/**
 * примеры извлечения значений
 */
public class ExtractTest {
    @Test
    public void extractTest() {
        String offset = RestAssured.given()
                .get(apiUrlFull() + "?offset=20")
                .then()
                .extract().body().path("MRData.offset");
        Assert.assertEquals("Wrong offset", "20", offset);
    }

    @Test
    public void extractTest2() {
        String offset = RestAssured.given()
                .get(apiUrlFull() + "?offset=20")
                .jsonPath().getString("MRData.offset");
        Assert.assertEquals("Wrong offset", "20", offset);
    }

    @Test
    public void extractList() {
        List<String> list = RestAssured.given()
                .get(apiUrlFull())
                .jsonPath().get("MRData.DriverTable.Drivers.driverId");
        Assert.assertEquals("Wrong size", 30, list.size());
    }

    @Test
    public void extractMap() {
        HashMap<String, Object> map = RestAssured.given()
                .get(apiUrlFull())
                .jsonPath().get("MRData.DriverTable.Drivers[0]");
        Assert.assertTrue("Must have key", map.containsKey("driverId"));
    }
}
