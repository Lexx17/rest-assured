package ru.restqa;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.restqa.Api.apiUrlFull;

/**
 * примеры сериализации и десериализации
 */
public class SerializeTest {
    @Test
    public void serializeTest() {
        SimpleBody simpleBody = new SimpleBody();
        simpleBody.setFirstName("John");
        RestAssured.given() // RequestSpecification
                .contentType(ContentType.JSON)
                .body(simpleBody)
                .get(apiUrlFull()) // Response
                .then() // Validatable
                .statusCode(200)
        ;
    }

    @Test
    public void serializeMapTest() {
        Map<String, Object> jsonAsMap = new HashMap<>();
        jsonAsMap.put("firstName", "John");

        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(jsonAsMap)
                .get(apiUrlFull())
                .then()
                .statusCode(200);
    }

    @Test
    public void deserializeDrivers() {
        List<Driver> list = RestAssured.given()
                .get(apiUrlFull())
                .jsonPath().getList("MRData.DriverTable.Drivers", Driver.class);
        Assert.assertEquals("Wrong id", "abate", list.get(0).getDriverId());
    }

    @Test
    public void deserializeDriver() {
        Driver driver = RestAssured.given()
                .get(apiUrlFull())
                .jsonPath().getObject("MRData.DriverTable.Drivers[0]", Driver.class);
        Assert.assertEquals("Wrong id", "abate", driver.getDriverId());
    }

    private class SimpleBody {
        private String firstName;

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }
    }
}
