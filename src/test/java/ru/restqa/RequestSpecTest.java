package ru.restqa;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.Cookies;
import io.restassured.specification.RequestSpecification;
import org.junit.Test;

import static ru.restqa.Api.*;

/**
 * примеры различных настроек запроса
 */
public class RequestSpecTest {
    @Test
    public void exampleTest() {
        Cookies testCookies = new Cookies();

        RestAssured.given()
                .header("Connection", "Keep-Alive")
                .header("Content-Type", "application/json")
                .cookies(testCookies)
                .formParam("login", "test")
                .get(apiUrlFull())
                .then()
                .statusCode(200)
        ;
    }

    @Test
    public void exampleBodyTest() {
        RestAssured.given()
                .header("Connection", "Keep-Alive")
                .header("Content-Type", "application/json")
                .body("{\"param1\":\"value1\"}")
                .get(apiUrlFull())
                .then()
                .statusCode(200)
        ;
    }

    @Test
    public void exampleAuthTest() {
        RestAssured.given()
                .log().all()
                .then().log().all()
                .request()
                .auth().basic("login1", "pass1")
                .header("Connection", "Keep-Alive")
                .header("Content-Type", "application/json")
                .get(apiUrlFull())
                .then()
                .statusCode(200)
        ;
    }

    @Test
    public void exampleSpecTest() {
        RequestSpecification rqSpec = new RequestSpecBuilder().build()
                .param("limit", 5)
                .port(apiPort())
                .log().all()
                //.then().log().all() null pointer
                .request();

        RestAssured.given()
                .spec(rqSpec)
                .param("offset", 20)
                .get(apiUrlFull())
                .then()
                .statusCode(200);
    }

    @Test
    public void exampleSpecTest2() {
        RequestSpecification rqSpec = RestAssured.given()
                .param("limit", 5)
                .port(apiPort())
                .log().all()
                .then().log().all()
                .request();

        RestAssured.given()
                .spec(rqSpec)
                .param("offset", 20)
                .get(apiUrlFull())
                .then()
                .statusCode(200);
    }

    @Test
    public void changeDefault() {
        RestAssured.baseURI = apiUrl();

        RestAssured.given()
                .log().all()
                .get("/drivers.json")
                .then()
                .statusCode(200);

        RestAssured.reset();
    }
}
