package ru.restqa;

import io.restassured.RestAssured;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasSize;
import static ru.restqa.Api.apiUrl;
import static ru.restqa.Api.apiUrlFull;

/**
 * простые примеры
 */
public class SimpleTest {
    @Test
    public void simpleTest() {
        RestAssured.given() // RequestSpecification
                .get(apiUrlFull()) // Response
                .then() // Validatable
                .statusCode(200)
        ;
    }

    @Test
    public void simpleTest2() {
        RestAssured.given()
                .response() // ResponseSpecification
                .statusCode(200)
                .with() // RequestSpecification
                .get(apiUrlFull()) // Response
        ;
    }

    @Test
    public void parametersTest() {
        int limit = 9;
        given()
                .param("limit", limit)
                .param("offset", 20)
                .get(apiUrlFull())
//        эквивалент
//                .get(apiUrlFull() + "?limit=10&offset=20")
                .then()
                .assertThat().statusCode(200)
                .and()
                .assertThat().body("MRData.DriverTable.Drivers.driverId", hasSize(limit))
        ;
    }

    @Test
    public void parametersTest2() {
        RestAssured.given()
                .pathParam("endpoint", "drivers.json")
                .response() // ResponseSpecification
                .statusCode(200)
                .with() // RequestSpecification
                .get(apiUrl() + "{endpoint}") // Response
        //        эквивалент
//                .get(apiUrl() + "drivers.json")
        ;
    }
}
