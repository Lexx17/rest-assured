package ru.restqa;

import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.specification.ResponseSpecification;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static ru.restqa.Api.apiUrlFull;

/**
 * примеры различных настроек проверок ответа
 */
public class ResponseSpecTest {
    @Test
    public void exampleTest() {
        RestAssured.given()
                .get(apiUrlFull())
                .then()
                .assertThat()
                .statusCode(200)
                .body("MRData.DriverTable", Matchers.notNullValue())
                //.header().cookies()
                .and().contentType(ContentType.JSON)
        ;
    }

    @Test
    public void specTest() {
        ResponseSpecification rsSpec = new ResponseSpecBuilder().build()
                .statusCode(200);

        RestAssured.given() // RequestSpecification
                .response() // ResponseSpecification
                .spec(rsSpec)
                .body("$", Matchers.notNullValue())
                .request() // RequestSpecification
                .get(apiUrlFull()) // Response
                .then() // Validatable
                .body("MRData.DriverTable", Matchers.notNullValue())
                .time(Matchers.lessThan(10L), TimeUnit.SECONDS)
                .and().contentType(ContentType.JSON)
        ;
    }

    @Test
    public void schemaTest() {
        RestAssured.given()
                .get(apiUrlFull())
                .then()
                .body(JsonSchemaValidator.matchesJsonSchemaInClasspath("driversSchema.json"))
        ;
    }

    @Test
    public void groovyTest() {
        RestAssured.given()
                .get(apiUrlFull())
                .then().body(

                "MRData.DriverTable.Drivers.findAll { it.driverId == 'andretti' }.givenName",
                Matchers.hasItems("Michael")
        );
    }

    @Test
    public void objectMatcherTest() {
        Matcher<?> mrDataMatcher = Matchers.allOf(
                Matchers.hasEntry("limit", "10"),
                Matchers.hasEntry("offset", "20")
        );

        RestAssured.given()
                .get(apiUrlFull() + "?limit=10&offset=20")
                .then().body("MRData", mrDataMatcher)
        ;
    }
}
