package ru.restqa;

import java.net.Socket;

public class Api {
    private static final boolean isLocal = isLocalWorked();

    public static String apiUrl() {
        return isLocal ?
                "http://localhost:8087/"
                : "http://ergast.com/api/f1/";
    }

    public static String apiUrlFull() {
        return isLocal ?
                "http://localhost:8087/drivers.json"
                : "http://ergast.com/api/f1/drivers.json";
    }

    public static int apiPort() {
        return isLocal ? 8087 : 80;
    }

    private static boolean isLocalWorked() {
        try (Socket socket = new Socket("localhost", 8087)) {
            return socket.isConnected();
        } catch (Exception e) {
            return false;
        }
    }
}
